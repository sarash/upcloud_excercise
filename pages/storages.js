import React, { Component } from 'react';
import Head from 'next/head';
import Loader from '../components/Loader';
import {getStorages} from '../api/storage';


class Storages extends Component {
    constructor(props){
        super(props);
        this.state = {
          storages: [],
          isLoading: true,
          error: null
        }
    }
    UNSAFE_componentWillMount(){
        getStorages()
        .then(res=>{
          this.setState({
            storages: res.data,
            isLoading: false
          })
        })
        .catch(err=>this.setState({
            error: err
        }))
    }
  render() {
      const storages = this.state.isLoading ? <span className="loader"><Loader /></span> : this.state.storages.map(storage=>(
          <li className="list-group-item" key={storage.uuid}>{storage.title}<span className="text-muted">({storage.size} GB)</span></li>
      ))
    return (
        <div>
            <Head>
                <title>Upcloud-Storages</title>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
            </Head>
            <main>
                <div className="storage-container">
                <ul className="list-group">
                    <li className="list-group-item"><h4>Storages</h4></li>
                    {this.state.error !== null ?
                    <div className="alert alert-danger" role="alert">
                        Oops!!! Something went wrong
                    </div> : storages}
                </ul>
                </div>
            </main>
        </div>
    )
  }
}

export default Storages;