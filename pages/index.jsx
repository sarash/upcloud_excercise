import React from 'react';
import Head from 'next/head';
// import Welcome from 'components/Welcome';
import {getServers} from '../api/server';
import Loader from '../components/Loader';


class Home extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      servers: [],
      isLoading: true,
      error: null
    }
  }
  UNSAFE_componentWillMount(){
    getServers()
    .then(res=>{
      this.setState({
        servers: res.data,
        isLoading: false
      })
    })
    .catch(err=>{
      this.setState({
        error: err
      })
    })
  }
  render(){
    const servers = this.state.isLoading ? <span className="loader"><Loader /></span> : this.state.servers.map(server=>(
        <li className="list-group-item server-lists" key={server.uuid}>
          <div className="square-icon">
            {
              (()=>{
                switch(server.state){
                  case 'started': return <i className="fas fa-square square-green"></i>
                  case 'stopped': return <i className="fas fa-square square-red" fa-size="3x"></i>
                  default: return <i className="fas fa-square"></i>
                }
              })()
            }           
            
          </div>
          <div className="server-detail">
            <strong><span>{server.title}</span></strong>
             <p className="text-muted">{server.hostname}</p>
          </div>
        </li>
    ))
    return(
      <div>
      <Head>
        <title>UpCloud-Servers</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"></link>
      </Head>
      <main>
        {/* TODO: render servers etc. ? */}
        <div className="server-container">
          <ul className="list-group">
            <li className="list-group-item"><h4>Servers</h4></li>
            {this.state.error !== null ?
              <div className="alert alert-danger" role="alert">
                Oops!!! Something went wrong
              </div> : servers}
          </ul>
        </div>
        {/* <Welcome /> */}
      </main>
    </div>
    )
  }
}

export default Home;
